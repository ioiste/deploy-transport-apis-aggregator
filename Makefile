clone-backend:
	rm -rf ./backend && git clone git@gitlab.com:ioiste/transport-apis-aggregator.git backend
clone-frontend:
	rm -rf ./frontend && git clone git@gitlab.com:ioiste/ui-transport-apis-aggregator.git frontend
clone:
	make clone-backend && make clone-frontend
pull-backend:
	cd backend && git reset --hard && git pull origin master
pull-frontend:
	cd frontend && git reset --hard && git pull origin master
pull:
	make pull-backend pull-frontend

build-prod:
	sudo rm -rf ./frontend/node_modules && docker-compose -f docker-compose.yml -f docker-compose.prod.yml build --parallel --force-rm --no-cache --pull
build-stg:
	sudo rm -rf ./frontend/node_modules && docker-compose -f docker-compose.yml -f docker-compose.stg.yml build --parallel --force-rm --no-cache --pull
build-dev-fresh:
	sudo rm -rf ./frontend/node_modules && docker-compose -f docker-compose.yml -f docker-compose.dev.yml build --parallel --force-rm --no-cache --pull
build-dev:
	docker-compose -f docker-compose.yml -f docker-compose.dev.yml build --parallel
start-prod:
	docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d
start-stg:
	docker-compose -f docker-compose.yml -f docker-compose.stg.yml up -d
start-dev:
	docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d && docker exec -it backend composer update
stop:
	docker-compose -f docker-compose.yml  -f docker-compose.dev.yml -f docker-compose.stg.yml -f docker-compose.prod.yml down --remove-orphans

phpcbf:
	docker exec -it backend php vendor/bin/phpcbf /var/www/html/src
phpcs:
	docker exec -it backend php vendor/bin/phpcs /var/www/html/src
phpstan:
	docker exec -it backend php vendor/bin/phpstan analyse src
