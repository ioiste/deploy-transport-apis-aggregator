# Integrator API-uri de Transport
To get the applications (API + UI) running:
* Install docker (with post-installation steps)
* Install docker-compose
* Install make
* Generate a ssh key & add it to GitLab
* Clone this repository
* Create `.env`
* For first time prod deployment run `make clone build-prod start-prod`

More commands are available in the Makefile.
